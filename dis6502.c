#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>

extern int opterr;

static int addr_mode_len[14] = {
	-1,	/* 0,  INVALID */
	0,	/* 1,  Accumulator */
	2,	/* 2,  absolute */
	2,	/* 3,  absolute, X-indexed */
	2,	/* 4,  absolute, Y-indexed */
	1,	/* 5,  immediate */
	0,	/* 6,  implied */
	2,	/* 7,  indirect */
	1,	/* 8,  X-indexed, indirect */
	1,	/* 9,  indirect, Y-indexed */
	1,	/* 10, relative */
	1,	/* 11, zeropage */
	1,	/* 12, zeropage, X-indexed */
	1	/* 13, zeropage, Y-indexed */
};

struct opcode_t {
	char *name;
	int addr_mode;
};

static struct opcode_t instr_set[256] = {
	{"BRK", 6 }, {"ORA", 8 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"ORA", 11}, {"ASL", 11}, {NULL, 0}, /* 007, 0x07 */
	{"PHP", 6 }, {"ORA", 5 }, {"ASL", 1 }, {NULL, 0}, {NULL,  0 }, {"ORA", 2 }, {"ASL", 2 }, {NULL, 0}, /* 015, 0x0F */

	{"BPL", 10}, {"ORA", 9 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"ORA", 12}, {"ASL", 12}, {NULL, 0}, /* 023, 0x17 */
	{"CLC", 6 }, {"ORA", 4 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"ORA", 3 }, {"ASL", 3 }, {NULL, 0}, /* 031, 0x1F */

	{"JSR", 2 }, {"AND", 8 }, {NULL,  0 }, {NULL, 0}, {"BIT", 11}, {"AND", 11}, {"ROL", 11}, {NULL, 0}, /* 039, 0x27 */
	{"PLP", 6 }, {"AND", 5 }, {"ROL", 1 }, {NULL, 0}, {"BIT", 2 }, {"AND", 2 }, {"ROL", 2 }, {NULL, 0}, /* 047, 0x2F */

	{"BMI", 10}, {"AND", 9 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"AND", 12}, {"ROL", 12}, {NULL, 0}, /* 055, 0x37 */
	{"SEC", 6 }, {"AND", 4 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"AND", 3 }, {"ROL", 3 }, {NULL, 0}, /* 063, 0x3F */

	{"RTI", 6 }, {"EOR", 8 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"EOR", 11}, {"LSR", 11}, {NULL, 0}, /* 071, 0x47 */
	{"PHA", 6 }, {"EOR", 5 }, {"LSR", 1 }, {NULL, 0}, {"JMP", 2 }, {"EOR", 2 }, {"LSR", 2 }, {NULL, 0}, /* 079, 0x4F */

	{"BVC", 10}, {"EOR", 9 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"EOR", 12}, {"LSR", 12}, {NULL, 0}, /* 087, 0x57 */
	{"CLI", 6 }, {"EOR", 4 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"EOR", 3 }, {"LSR", 3 }, {NULL, 0}, /* 095, 0x5F */

	{"RTS", 6 }, {"ADC", 8 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"ADC", 11}, {"ROR", 11}, {NULL, 0}, /* 103, 0x67 */
	{"PLA", 6 }, {"ADC", 5 }, {"ROR", 1 }, {NULL, 0}, {"JMP", 7 }, {"ADC", 2 }, {"ROR", 2 }, {NULL, 0}, /* 111, 0x6F */

	{"BVS", 10}, {"ADC", 9 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"ADC", 12}, {"ROR", 12}, {NULL, 0}, /* 119, 0x77 */
	{"SEI", 6 }, {"ADC", 4 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"ADC", 3 }, {"ROR", 3 }, {NULL, 0}, /* 127, 0x7F */

	{NULL,  0 }, {"STA", 8 }, {NULL,  0 }, {NULL, 0}, {"STY", 11}, {"STA", 11}, {"STX", 11}, {NULL, 0}, /* 135, 0x87 */
	{"DEY", 6 }, {NULL,  0 }, {"TXA", 6 }, {NULL, 0}, {"STY", 2 }, {"STA", 2 }, {"STX", 2 }, {NULL, 0}, /* 143, 0x8F */

	{"BCC", 10}, {"STA", 9 }, {NULL,  0 }, {NULL, 0}, {"STY", 12}, {"STA", 12}, {"STY", 13}, {NULL, 0}, /* 151, 0x97 */
	{"TYA", 6 }, {"STA", 4 }, {"TXS", 6 }, {NULL, 0}, {NULL,  0 }, {"STA", 3 }, {NULL,  0 }, {NULL, 0}, /* 159, 0x9F */

	{"LDY", 5 }, {"LDA", 8 }, {"LDX", 5 }, {NULL, 0}, {"LDY", 11}, {"LDA", 11}, {"LDX", 11}, {NULL, 0}, /* 167, 0xA7 */
	{"TAY", 6 }, {"LDA", 5 }, {"TAX", 6 }, {NULL, 0}, {"LDY", 2 }, {"LDA", 2 }, {"LDX", 2 }, {NULL, 0}, /* 175, 0xAF */

	{"BCS", 10}, {"LDA", 9 }, {NULL,  0 }, {NULL, 0}, {"LDY", 12}, {"LDA", 12}, {"LDX", 13}, {NULL, 0}, /* 183, 0xB7 */
	{"CLV", 6 }, {"LDA", 4 }, {"TSX", 6 }, {NULL, 0}, {"LDY", 3 }, {"LDA", 3 }, {"LDX", 4 }, {NULL, 0}, /* 191, 0xBF */

	{"CPY", 5 }, {"CMP", 8 }, {NULL,  0 }, {NULL, 0}, {"CPY", 11}, {"CMP", 11}, {"DEC", 11}, {NULL, 0}, /* 199, 0xC7 */
	{"INY", 6 }, {"CMP", 5 }, {"DEX", 6 }, {NULL, 0}, {"CPY", 2 }, {"CMP", 2 }, {"DEC", 2 }, {NULL, 0}, /* 207, 0xCF */

	{"BNE", 10}, {"CMP", 9 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"CMP", 12}, {"DEC", 12}, {NULL, 0}, /* 215, 0xD7 */
	{"CLD", 6 }, {"CMP", 4 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"CMP", 3 }, {"DEC", 3 }, {NULL, 0}, /* 223, 0xDF */

	{"CPX", 5 }, {"SBC", 8 }, {NULL,  0 }, {NULL, 0}, {"CPX", 11}, {"SBC", 11}, {"INC", 11}, {NULL, 0}, /* 231, 0xE7 */
	{"INX", 6 }, {"SBC", 5 }, {"NOP", 6 }, {NULL, 0}, {"CPX", 2 }, {"SBC", 2 }, {"INC", 2 }, {NULL, 0}, /* 239, 0xEF */

	{"BEQ", 10}, {"SBC", 9 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"SBC", 12}, {"INC", 12}, {NULL, 0}, /* 247, 0xF7 */
	{"SED", 6 }, {"SBC", 4 }, {NULL,  0 }, {NULL, 0}, {NULL,  0 }, {"SBC", 3 }, {"INC", 3 }, {NULL, 0}, /* 255, 0xFF */
};

int main(int argc, char **argv)
{
	int c;
	char *file_str;
	FILE *infile;
	unsigned char instr;
	unsigned char parms[2];
	int addr_mode;
	char *instr_str;
	size_t bytecount = 0;

	opterr = 0;

	while ((c = getopt(argc, argv, "i:")) != -1) {
		switch (c) {
		case 'i':
			file_str = optarg;
			break;

		default:
			printf("no file!\n");
			exit(EXIT_FAILURE);
			break;
		}
	}

	infile = fopen(file_str, "rb");

	if (!infile) {
		printf("File not found: %s\n", file_str);
		exit(EXIT_FAILURE);
	}

	while (!feof(infile)) {
		if(bytecount += fread(&instr, 1, 1, infile)) {
			instr_str = instr_set[instr].name;
			printf("%08zx:\t", bytecount - 1);
			if (instr_str) {
				addr_mode = instr_set[instr].addr_mode;
				bytecount += fread(parms, addr_mode_len[addr_mode], 1, infile);
				switch (addr_mode) {
				case 0:
					printf("INVALID\n");
					break;
				case 1:
					printf("%s A\t\t\t\t0x%02X\n", instr_str, instr);
					break;
				case 2:
					printf("%s $%02X%02X\t\t\t0x%02X 0x%02X 0x%02X\n",
					       instr_str, parms[1], parms[0], instr, parms[0], parms[1]);
					break;
				case 3:
					printf("%s $%02X%02X,X\t\t\t0x%02X 0x%02X 0x%02X\n",
					       instr_str, parms[1], parms[0], instr, parms[0], parms[1]);
					break;
				case 4:
					printf("%s $%02X%02X,Y\t\t\t0x%02X 0x%02X 0x%02X\n",
					       instr_str, parms[1], parms[0], instr, parms[0], parms[1]);
					break;
				case 5:
					printf("%s #$%02X\t\t\t0x%02X 0x%02X\n",
					       instr_str, parms[0], instr, parms[0]);
					break;
				case 6:
					printf("%s\t\t\t\t0x%02X\n", instr_str, instr);
					break;
				case 7:
					printf("%s ($%02X%02X)\t\t\t0x%02X 0x%02X 0x%02X\n",
					       instr_str, parms[1], parms[0], instr, parms[0], parms[1]);
					break;
				case 8:
					printf("%s ($%02X,X)\t\t\t0x%02X 0x%02X\n",
					       instr_str, parms[0], instr, parms[0]);
					break;
				case 9:
					printf("%s ($%02X),Y\t\t\t0x%02X 0x%02X\n",
					       instr_str, parms[0], instr, parms[0]);
					break;
				case 10:
					printf("%s $%02X\t\t\t\t0x%02X 0x%02X\n",
					       instr_str, parms[0], instr, parms[0]);
					break;
				case 11:
					printf("%s $%02X\t\t\t\t0x%02X 0x%02X\n",
					       instr_str, parms[0], instr, parms[0]);
					break;
				case 12:
					printf("%s $%02X,X\t\t\t0x%02X 0x%02X\n",
					       instr_str, parms[0], instr, parms[0]);
					break;
				case 13:
					printf("%s $%02X,Y\t\t\t0x%02X 0x%02X\n",
					       instr_str, parms[0], instr, parms[0]);
					break;
				}
			} else {
				/* BAD INSTRUCTION */
				printf("--\t\t\t\t0x%02X\n", instr);
			}
		}
	}

	fclose(infile);

	return 0;
}
