# dis6502

A program for Linux that takes an input binary file of 6502 machine code and
outputs the disassembled 6502 assembly code.

Usage: dis6502 -i test.bin

Instruction set obtained from:

http://e-tradition.net/bytes/6502/6502_instruction_set.html
