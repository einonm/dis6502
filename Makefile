PROG = dis6502

CC = gcc
CFLAGS = -Wall -g

OBJ = dis6502.o

all: $(PROG)

$(PROG) : $(OBJ)
	$(CC) $(LDFLAGS) -o bin/$@ $(OBJ)



